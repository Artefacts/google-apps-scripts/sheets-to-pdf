# Sheets to PDF

Exporter chaque feuille (sheet) d'un classeur (spreadsheet) dans un pdf individuel

## Doc

### Apps Script

- https://developers.google.com/apps-script/reference?hl=fr

### FTP/SFTP

Il semble que Google Apps Script ne peut communiquer qu'en HTTP
- https://developers.google.com/apps-script/reference/url-fetch/url-fetch-app?hl=fr

Un connecter SFTP ou FTP existe pour Google Cloud (en Pre-GA Offerings Terms)
- https://cloud.google.com/integration-connectors/docs/connectors/ftp/configure
