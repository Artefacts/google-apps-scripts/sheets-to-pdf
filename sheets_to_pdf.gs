function Sheets_to_PDF()
{
  var spreadsheet = SpreadsheetApp.getActive();

  var parents = DriveApp.getFileById(spreadsheet.getId()).getParents();
  var folder ;
  if (parents.hasNext()) {
    folder = parents.next();
  }
  else {
    folder = DriveApp.getRootFolder();
  }
  //Logger.log( 'folder: ' + folder.getName() );

  var formattedDate = Utilities.formatDate(new Date(), "Europe/Paris", "yyyy-MM-dd HH:mm");

  var tempSpreadsheet = SpreadsheetApp.create("temp");

  var sheets = spreadsheet.getSheets();
  for (i = 0; i < sheets.length; i++)
  {

    var pdfName = spreadsheet.getName() + ' - ' + sheets[i].getSheetName() + ' - ' + formattedDate+'.pdf' ;
    tempSpreadsheet.toast("Génération de "+pdfName, "Ça travaille...");
    //Logger.log( 'pdfName: ' + pdfName );

    sheets[i].copyTo(tempSpreadsheet);
    tempSpreadsheet.deleteSheet(tempSpreadsheet.getSheets()[0]);

    var theBlob = tempSpreadsheet.getBlob().getAs('application/pdf').setName( pdfName );
    var newFile = folder.createFile(theBlob);
  }

  tempSpreadsheet.toast("Les PDFs sont dans le dossier: "+folder.getName(), "Voilà, c'est fait.");

};
